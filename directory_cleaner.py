from datetime import datetime
import subprocess
from subprocess import Popen
import glob
import os

FILE_TYPES = [".dmg"]
DIR = "/Users/enzo/Downloads"


class DirectoryCleaner():

    def __init__(self):
        self.files = []
        self.removable_files = []
        self.files = self.get_files(FILE_TYPES)
        self.removable_files = self.get_removable_files(self.files)
        if self.removable_files:
            self.delete_files(self.removable_files)

    def get_files(self, file_types):
        """
        Loop through all the file types and append the files array with valid files in the directory provided
        in the DIR variable.
        """
        self.files = []
        for t in file_types:
            self.files.append(glob.glob(self.build_dir(t, DIR)))
        return self.files

    def build_dir(self, filetype, dir=DIR):
        self.my_directory = DIR + "/*{}".format(filetype)
        return self.my_directory

    def get_removable_files(self, file_set):
        self.valid_files = []
        self.today = datetime.today()
        for s in file_set:
            for f in s:
                self.create_date_timestamp = os.stat(f).st_ctime
                self.create_date = datetime.fromtimestamp(self.create_date_timestamp)
                self.time_diff_days = (self.today - self.create_date).days
                if self.time_diff_days > 7:
                    self.valid_files.append(f)
        return self.valid_files

    def delete_files(self, valid_files):
        cmd = ["rm"] + self.valid_files
        self.output = ""
        self.error = ""
        p = Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.output, self.error = p.communicate()
        print(self.output)
        print(self.error)
        return True


dc = DirectoryCleaner()
